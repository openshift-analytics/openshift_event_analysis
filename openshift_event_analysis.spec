# Created by pyp2rpm-3.2.3
%global pypi_name openshift-event-analysis

Name:           python-%{pypi_name}
Version:        0.4.0
Release:        1%{?dist}
Summary:        Some basic Classes to be use with OpenShift and Apache Kafka

License:        GPLv3+
URL:            https://gitlab.com/openshift-analytics/openshift_event_analysis
Source0:        https://files.pythonhosted.org/packages/source/o/%{pypi_name}/openshift_event_analysis-%{version}.tar.gz
BuildArch:      noarch
 
BuildRequires:  python3-devel
BuildRequires:  python3-check-manifest
BuildRequires:  python3-setuptools
BuildRequires:  python3-twine

%description
This is a set of Classes to be used with OpenShift and Kafka. The purpose if to
provide basic functions to receive OpenShift and Kubernetes Events via Apache
Kafka.

%package -n     python3-%{pypi_name}
Summary:        %{summary}
%{?python_provide:%python_provide python3-%{pypi_name}}
 
Requires:       python3-kafka
Requires:       python3-prometheus-client
Requires:       python3-coverage
Requires:       python3-flake8
Requires:       python3-pytest
Requires:       python3-requests
%description -n python3-%{pypi_name}
This is a set of Classes to be used with OpenShift and Kafka. The purpose if to
provide basic functions to receive OpenShift and Kubernetes Events via Apache
Kafka.


%prep
%autosetup -n openshift_event_analysis-%{version}
# Remove bundled egg-info
rm -rf %{pypi_name}.egg-info

%build
%py3_build

%install
%py3_install


%files -n python3-%{pypi_name}
%doc README.md
%{python3_sitelib}/openshift_event_analysis-%{version}-py?.?.egg-info

%changelog
* Wed Oct 04 2017 Christoph Görn <goern@redhat.com> - 0.4.0-1
- Initial package.
